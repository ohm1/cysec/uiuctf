import model
import numpy as np
import random
from PIL import Image

HASH_DIFF = 3


def mutate(px):
    return (1 + (random.random() - 0.5)) * px


def evolve_image(im):
    return np.clip(np.apply_along_axis(mutate, 2, im), 0, 255)


def disguise_pony(pony):
    predictor = model.Predictor(pony)

    current = np.asarray(pony)
    score = predictor.get_prediction(pony)
    generation = 0

    while score < 0:
        # randomly mutate the image
        evolved = evolve_image(current).astype(np.uint8)
        generation += 1
        # convert from np array to PIL object
        evolved_img = Image.fromarray(evolved)
        # check that the difference in hashes is not too big
        hash_diff = predictor.compare_hash(evolved_img)
        if hash_diff > 3:
            continue
        # if we did better set the current prediction to this one and update the score
        new_score = predictor.score_image(evolved_img)
        if new_score > score:
            current = evolved
            score = new_score
            print(f"generation {generation}: hashdiff = {hash_diff}, score = {score}")
    return Image.fromarray(current)


def main():
    pony = Image.open("/home/timothy/documents/pony.png")
    disguise_pony(pony).save("out.png")


if __name__ == "__main__":
    main()
