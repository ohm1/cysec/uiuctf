import imagehash

from torchvision import models
import torchvision.transforms as transforms
import torch.nn as nn
import torch
import numpy as np

# ------------------ Model goes here ⬇------------------ #
imagenet_class_index = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship',
                        'truck']
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model_nonrobust = models.resnet18()
num_ftrs = model_nonrobust.fc.in_features
model_nonrobust.fc = nn.Linear(num_ftrs, len(imagenet_class_index))
model_nonrobust.load_state_dict(
    torch.load(PATH_TO_MODEL_GOES_HERE,
               map_location=device))
model_ft = model_nonrobust.to(device)
model_nonrobust.eval()


# Transform image to normalize into model's bounds
def transform_image(image):
    my_transforms = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(
            [0.485, 0.456, 0.406],
            [0.229, 0.224, 0.225])])
    return my_transforms(image).unsqueeze(0)


class Predictor:
    def __init__(self, image):
        """
        :param image: a PIL object
        """
        self.original_hash = imagehash.average_hash(image)

    def score_image(self, image):
        inputs = transform_image(image)
        if torch.cuda.is_available():
            inputs = inputs.cuda()
        outputs = model_nonrobust(inputs)
        class_scores = outputs.cpu().detach().numpy()[0]
        # get difference between highest non-horse score and horse score
        return max(np.delete(class_scores, 7)) - class_scores[7]

    def compare_hash(self, new_image):
        input_image = new_image
        input_hash = imagehash.average_hash(input_image)
        return self.original_hash - input_hash
