#!/usr/bin/env python3
from base64 import b64decode, b64encode
from io import BytesIO

from bs4 import BeautifulSoup
from PIL import Image
import requests

from solve import disguise_pony as get_solution_image

SITE_URL = 'http://pwnies-please.chal.uiuc.tf/'


class PwniesSession:

    def __init__(self):
        self.session = requests.Session()

    def get_page(self):
        response = self.session.get(SITE_URL)
        return response.text

    def get_image(self):
        response = self.session.get(SITE_URL)
        soup = BeautifulSoup(response.content, 'html.parser')
        img_src = soup.find('img').attrs['src']
        message = soup.find('div', id='response').string
        _, b64_img = img_src.split(',')
        return Image.open(BytesIO(b64decode(b64_img))), message

    def upload_image(self, image):
        img_bytes_io = BytesIO()
        image.save(img_bytes_io, format='PNG')
        img_bytes_io.seek(0)
        response = self.session.post(f'{SITE_URL}?', files={'file': ('image.png', img_bytes_io, 'image/png')})
        soup = BeautifulSoup(response.content, 'html.parser')
        return soup.find('div', id='response').string


def main():
    session = PwniesSession()
    while True:
        image, message = session.get_image()
        print(f'{message}\n')
        solution_image = get_solution_image(image)
        message = session.upload_image(solution_image)
        print(f'\n{message}\n')


if __name__ == '__main__':
    main()
